#!/usr/bin/env python3
import sys
import pwd
import argparse
import locale
import xdg.Locale
import os
import shlex
from subprocess import Popen, PIPE
from multiprocessing import Process, Queue
import re
import xdg.Menu
import xdg.DesktopEntry
import xdg.IconTheme
import xdg.Config
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import grp

opts = argparse.Namespace()
valid_shells=['/bin/bash','/usr/bin/bash','/bin/sh','/usr/bin/sh']
default_locale = "en_US.UTF-8"
encoding = locale.getlocale()[1] or 'UTF-8'

#Class to print various messages
class Message():
    def Success(message):
        print(message)

    def Error(message,error_code):
        print("Error: %s" % message)
        sys.exit(error_code)

    def Warn(message):
        print("Warning: %s" % message)

    def Debug(message):
        print("Debug: %s" % message)


#Class to drop out of root:root to another effective user:group and back again on exit
class AsUser():
    def __init__(self,uid,gid):
        self.uid = uid
        self.gid = gid
    
    def __enter__(self):
        self.current_login = [os.geteuid(), os.getegid()]
        #Group id must be set before user id or permissions are forfeited before setting group
        os.setegid(self.gid)
        os.seteuid(self.uid)
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        os.setegid(self.current_login[1])
        os.seteuid(self.current_login[0])

#Class to set the variables based on global (as root / apt), standard (as a user) or common to both
class fill_namespace():
    def __init__(self): 
        opts.debug = True
        opts.users = {}
        opts.config_folder = "/.config/desktop-menu/"
        opts.template_folder = "/usr/share/desktop-menu/templates/"
        opts.locale_file = "locale.cfg"
        opts.global_write = True
        opts.menu_file = "/etc/xdg/menus/applications.menu"
        opts.root_folder = ""
        opts.write_out = True
        opts.no_gtk = False
        opts.icon_theme = Gtk.IconTheme.new()
        opts.icon_size = 48
        opts.menu_programs = False
        opts.exec_clean1_re = re.compile(r'%[a-zA-Z]')
        opts.exec_clean2_re = re.compile(r'%%')
        opts.terminal = "desktop-defaults-run -t %s"
        opts.missing_icon = "gtk-missing-image"
        opts.default_folder_icon = "folder"
        opts.default_entry_icon = "-"
        #Build Icon Location Array
        opts.icon_file_list=[]
        for path in ['/usr/share/icons', '/usr/share/pixmaps']:
            for (dirpath, dirnames, filenames) in os.walk(path):
                for filename in filenames:
                    if filename.endswith(( '.png', '.svg', '.xpm')):
                        opts.icon_file_list.append(dirpath+"/"+filename)
        if opts.debug: Message.Debug("Namespace init ran")
    
    def global_write_options():
        opts.menu_folder = "/var/cache/desktop-menu/"
        if not os.path.isdir(opts.menu_folder):
            os.mkdir(opts.menu_folder)
            os.chown(opts.menu_folder, 0, 100)
            os.chmod(opts.menu_folder, 0o774)

    def user_options(user,user_info):
        #Try to read locale from users home directory, else use default 
        try:
            locale_stored = open(user_info[2] + opts.config_folder + opts.locale_file)
        except: 
            Message.Warn("Cannot open locale file for %s \n Using default %s " % (user, default_locale))
            opts.locale = default_locale
        else:
            opts.locale = locale_stored.readline().strip()
            locale_stored.close()
        #Set user menu folder if global write
        if opts.global_write:
            opts.user_menu_folder = opts.menu_folder + user + "/"
            if not os.path.isdir(opts.user_menu_folder):
                os.mkdir(opts.user_menu_folder)
                os.chown(opts.user_menu_folder, user_info[0], user_info[1])
                os.chmod(opts.user_menu_folder, 0o774)

                
#Class to get the icon for the app based on the desktop info
class Desktop_Info:
    def find_icon(entry):
        appicon = entry.getIcon()
        icon = ""
        if os.path.isfile(appicon):
            icon = appicon
        else:
            if opts.icon_theme.lookup_icon(appicon, opts.icon_size, 0) and not opts.no_gtk:
                icon_info = opts.icon_theme.lookup_icon(appicon, opts.icon_size, 0)
                icon = icon_info.get_filename()
            else:
                for i, item in enumerate(opts.icon_file_list):
                    if re.search(appicon, item):
                        icon = item;
                        #Note: removing search for default icon size as preffered and immediately breaking after first matching instance can speed up script.
                        if re.search(str(opts.icon_size), item):
                            break
        if not icon:
            icon = opts.missing_icon

        icon = re.sub(r' ',' ', icon)
        return icon

class SUB_ORDER: 
  def __init__(self,menuORprog,NAME,ICON,EXECUTE):
    var = SUB_ORDER
    count=0
    if menuORprog == "menu":
      SUB = Template.Menu_Sub_Order.split(',')
    elif menuORprog == "prog":
      SUB = Template.Program_Sub_Order.split(',')
      
    for value in SUB:
      str_count=str(count)
      var.VARIABLE="sub"+str_count
      if re.search(r'icon', value):
        setattr(var, var.VARIABLE, ICON)
      elif re.search(r'execute', value):
        setattr(var, var.VARIABLE, EXECUTE)
      elif re.search(r'name', value):
        setattr(var, var.VARIABLE, NAME)
      count=count+1
      
#Class to read the template file of the passed window manager
class Template: 
  def read(self,wm):
    template = Template
    if opts.debug: Message.Debug("Reading template file: %s%s.template" % (opts.template_folder, wm))
    for line in open(opts.template_folder+wm+".template", "r"):
      if "#" not in line:
        if re.search(r'^.*=', line):
          pieces = line.split(' = ')
          template.VARIABLE=(pieces[0])
          template.VARIABLE = re.sub(r'\n', '', template.VARIABLE)
          OBJECT=(pieces[1])
          OBJECT = re.sub(r'\n', '', OBJECT)
          setattr(template, template.VARIABLE, OBJECT)

#Class to write the menu based on the window manager and user
class standard_write():
    def __init__(self,wm,user,user_info,xdg_menu_data):
        if opts.debug: Message.Debug("This is a standard write for %s" % wm)
        Template().read(wm)
        if opts.write_out:
            self.write_to_file("w", wm, Template.Separator)
        else:
            print(Template.File_Start)
        self.process_menu(xdg_menu_data,wm)
        if opts.write_out:
            self.write_to_file("a", wm, Template.Separator)
        else:
            print(Template.File_End)
        
    def write_to_file(self, write_type, wm, string):
        text = open((opts.user_menu_folder+wm), write_type)
        text.write (str(string)+"\n")
        text.close()

    def get_order(self,order, name, gname, cname):
        separator_left = "["
        separator_right = "]"
        
        if not gname or gname.lower() in name.lower():
            order=re.sub(r'g', '', order);
            gname="";
        if not name or name.lower() in gname.lower():
            order=re.sub(r'n', '', order);
            name="";
        if not cname or cname.lower() in name.lower() or cname.lower() in gname.lower():
            order=re.sub(r'c', '', order);
            cname="";
        
        order_dict={
            'ngc':  name    +" "+ separator_left + gname    + separator_right +" "+ separator_left + cname   + separator_right,
            'ncg':  name    +" "+ separator_left + cname    + separator_right +" "+ separator_left + gname   + separator_right,
            'cng':  cname   +" "+ separator_left + name     + separator_right +" "+ separator_left + gname   + separator_right,
            'cgn':  cname   +" "+ separator_left + gname    + separator_right +" "+ separator_left + name    + separator_right,
            'gnc':  gname   +" "+ separator_left + name     + separator_right +" "+ separator_left + cname   + separator_right,
            'gcn':  gname   +" "+ separator_left + cname    + separator_right +" "+ separator_left + name    + separator_right,
            'ng':   name    +" "+ separator_left + gname    + separator_right,
            'nc':   name    +" "+ separator_left + cname    + separator_right,
            'cn':   cname   +" "+ separator_left + name     + separator_right,
            'cg':   cname   +" "+ separator_left + gname    + separator_right,
            'gn':   gname   +" "+ separator_left + name     + separator_right,
            'gc':   gname   +" "+ separator_left + cname    + separator_right
        } 
        return order_dict.get(order, name + gname + cname)
       
    def process_menu(self,xdg_menu_data,wm):
        name="";  gname=""; cname="";
        program_names = True
        icons_only = False
        generic_names = False
        comments = False
        category_filter = "n,g,c"
        order=""
        no_icons = False
  
        for entry in xdg_menu_data.getEntries():
            #Check if menu item is a separator
            if isinstance(entry, xdg.Menu.Separator):
                if opts.write_out:
                    self.write_to_file("a", wm, Template.Separator)
                else:
                    print(Template.Separator)

            #Check if menu item is a another menu
            elif isinstance(entry, xdg.Menu.Menu):
                if program_names and not icons_only and "n" in category_filter:
                    name = entry.getName() or entry.DesktopFileID
                    if wm == 'fluxbox':
                        name = name.replace(')','\)')
      
                if generic_names and not icons_only and "g" in category_filter:
                    gname = entry.getGenericName()
                    if wm == 'fluxbox':
                        gname = gname.replace(')','\)')
      
                if comments and not icons_only and "c" in category_filter:
                    cname = entry.getComment()
                    if wm == 'fluxbox':
                        cname = cname.replace(')','\)')

                if icons_only:
                    ordered_text = ""
                else:
                    ordered_text = self.get_order(order,name,gname,cname)

                if not no_icons:
                    icon = Desktop_Info.find_icon(entry) or opts.default_folder_icon

                if opts.menu_programs:
                    print ((Template.Menu_Program % (ordered_text, icon, sys.argv[0])) +
                    (" --root-folder \"%s\"" % entry.getPath(org=True)) +
                    (" --terminal \"%s\"" % opts.terminal)).encode(encoding),
                    if not no_icons:
                        print ((" --default-folder-icon \"%s\"" % opts.default_folder_icon) +
                        (" --default-entry-icon \"%s\"" % opts.default_entry_icon) +
                        (" --theme \"%s\"" % xdg.Config.icon_theme) +
                        (" --icon-size \"%d\"" % opts.icon_size) +
                        (with_theme_paths and " --with-theme-paths" or "")).encode(encoding),
                    if locale_str:
                        print (" --locale \"%s\"" % locale_str).encode(encoding),
                    print()
                else:
                    if no_icons:
                        SUB_ORDER('menu',ordered_text,'','')
                    else:
                        SUB_ORDER('menu',ordered_text,icon,'')
                    if opts.write_out:
                        if Template.Menu_ID == '1':
                            self.write_to_file("a", wm, Template.Menu_Start % (SUB_ORDER.sub0, SUB_ORDER.sub1, SUB_ORDER.sub2))
                        else:
                            self.write_to_file("a", wm, Template.Menu_Start % (SUB_ORDER.sub0, SUB_ORDER.sub1))
                    else:
                        if Template.Menu_ID == '1':
                            print (Template.Menu_Start % (SUB_ORDER.sub0, SUB_ORDER.sub1, SUB_ORDER.sub2))
                        else:
                            print (Template.Menu_Start % (SUB_ORDER.sub0, SUB_ORDER.sub1))
                    self.process_menu(entry,wm)
                    if opts.write_out:
                        self.write_to_file("a", wm, Template.Menu_End)
                    else:
                        print (Template.Menu_End)
    
            #Check if menu item is an application
            elif isinstance(entry, xdg.Menu.MenuEntry):
                de = entry.DesktopEntry
                desktopFile=str(entry)
                OnlyShowIn = de.getOnlyShowIn()
                if not os.path.isfile("/usr/share/applications/antix/"+desktopFile) and not OnlyShowIn:
        
                    if program_names and not icons_only:
                        name = de.getName() or entry.DesktopFileID
                        if wm == 'fluxbox':
                            name = name.replace(')','\)')
        
                    if generic_names and not icons_only:
                        gname = de.getGenericName()
                        if wm == 'fluxbox':
                            gname = gname.replace(')','\)')
        
                    if comments and not icons_only:
                        cname = de.getComment()
                        if wm == 'fluxbox':
                            cname = cname.replace(')','\)')
        
                    if icons_only:
                        ordered_text = " "
                    else:
                        ordered_text = self.get_order(order,name,gname,cname)

                    execute = opts.exec_clean2_re.sub('%', opts.exec_clean1_re.sub('', de.getExec()))
                    if de.getTerminal(): execute = opts.terminal % execute      
                    if no_icons:
                        SUB_ORDER('prog',ordered_text,'',execute)
                    else:
                        icon = Desktop_Info.find_icon(de) or default_entry_icon
                        SUB_ORDER('prog',ordered_text,icon,execute)
                    if opts.write_out:
                        self.write_to_file("a", wm, Template.Program % (SUB_ORDER.sub0, SUB_ORDER.sub1, SUB_ORDER.sub2))
                    else:
                        print (Template.Program % (SUB_ORDER.sub0, SUB_ORDER.sub1, SUB_ORDER.sub2))
    

#Class to gather all the users under the user group and build each menu for each window manager as that user
class global_write():
    def __init__(self):
        fill_namespace.global_write_options()
        self.get_users()
        for user,user_info in opts.users.items():
            if opts.debug:
                Message.Debug("Username: %s" % (user))
                Message.Debug("Home Directory: %s" % (user_info[2]))
            fill_namespace.user_options(user,user_info)
            if opts.debug:
                Message.Debug("Locale: %s" % (opts.locale))
                Message.Debug("Menu Folder: %s" % (opts.user_menu_folder))

            with AsUser(user_info[0], 100):
                if opts.debug:
                    Message.Debug("Current UID: {}".format(os.geteuid()))
                    Message.Debug("Current GID: {}".format(os.getegid()))
                xdg_menu_data = xdg.Menu.parse(opts.menu_file)
                self.generate_menus(user,user_info,xdg_menu_data)

    def get_users(self):
        usergroup=grp.getgrgid(100)
        for user in usergroup[3]:
            user_info=pwd.getpwnam(user)  	
            if user_info[6] in valid_shells:
                opts.users[user] = [user_info[2], user_info[3], user_info[5], "", ""]

    def generate_menus(self, user,user_info,xdg_menu_data):
        for i in os.listdir('/usr/share/desktop-menu/templates'):
            if i.endswith(".template"):
                checkwmname = "which " + i.split('.',1)[0]
                myprocess = Popen( shlex.split(checkwmname), stdout=PIPE)
                mystdout = myprocess.communicate()[0]
                ### if the corresponding wm is not currently installed, skip this template
                ###  (to avoid logspam e.g. "Writing Menu: jwm... sh: 1: jwm: not found")
                if mystdout and mystdout[0] != '':
                    i = re.sub(r'\..*', '', i)
                    Message.Success ('Writing Menu: %s' % i)
                    p = Process(target=standard_write, args=(i,user,user_info,xdg_menu_data))
                    p.start()
                    p.join() # this blocks until the process terminates
        
fill_namespace()
if opts.global_write:
    if os.geteuid() != 0:
        Message.Error("You need to be root to write a menu globally to all users", 1)
    global_write()
else:
    standard_write()
    
